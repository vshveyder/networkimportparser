<?php

namespace NetworkImport;

class NetworkApi
{
    private $url = 'https://network.uloop.com';

    public function feedImportFileApiLink($fileName)
    {
        return "$this->url/feed-import-files/$fileName";
    }

    public function getFile($file)
    {
        return file_get_contents(
            $this->feedImportFileApiLink($file)
        );
    }
}