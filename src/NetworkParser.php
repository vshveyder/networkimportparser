<?php

namespace NetworkImport;

class NetworkParser
{
    private $property;

    public function __construct($property)
    {
        $this->property = $property;
    }

    public function id()
    {
        return isset($this->property['@attributes']) && isset($this->property['@attributes']['id'])
            ? $this->property['@attributes']['id']
            : '';
    }

    public function subcategory()
    {
        return $this->property['subcategory'];
    }

    public function code()
    {
        return $this->property['code'];
    }

    public function label()
    {
        return isset($this->property['label']) ? $this->property['label'] : '';
    }

    public function poweredBy()
    {
        return $this->property['import_id'];
    }

    public function title()
    {
        return $this->property['title'];
    }

    public function description()
    {
        return $this->property['content'] && (string) $this->property['content'] !== 'Array'
            ? nl2br(trim(str_replace('â', "'",(string) $this->property['content'])))
            : '';
    }

    public function address()
    {
        return $this->property['address'];
    }

    public function city()
    {
        return $this->property['city'];
    }

    public function region()
    {
        return $this->property['region'];
    }

    public function routeEmail()
    {
        return (string) $this->property['route_email'];
    }

    public function longitude()
    {
        return (float) $this->property['longitude'];
    }

    public function latitude()
    {
        return (float) $this->property['latitude'];
    }

    public function zipCode()
    {
        return $this->property['postcode'] ?: '';
    }

    public function pictures()
    {
        if (!isset($this->property['pictures']) || !isset($this->property['pictures']['picture'])) {
            return [];
        }

        return is_array($this->property['pictures']['picture']) && count($this->property['pictures']['picture'])
            ? $this->property['pictures']['picture']
            : [$this->property['pictures']['picture']];
    }

    public function units()
    {
        if (!isset($this->property['units']) || !isset($this->property['units']['unit'])) {
            return [];
        }

        if (!isset($this->property['units']['unit'][0])) {
            $this->property['units']['unit'] = [$this->property['units']['unit']];
        }

        return array_map(function ($unit) {
            return (object) [
                'id' => isset($unit['@attributes']) && isset($unit['@attributes']['id']) ? $unit['@attributes']['id'] : '',
                'title' => (string) $unit['title'],
                'description' => (string) $unit['description'],
                'bedrooms' => $unit['bedrooms'] ? (string) $unit['bedrooms'] : 0,
                'bathrooms' => $unit['bathrooms'] ? (string) $unit['bathrooms'] : 0,
                'minRent' => $unit['minRent'],
                'maxRent' => $unit['maxRent'],
                'minSize' => $unit['minSize'],
                'maxSize' => $unit['maxSize'],
                'picture' => (array) $unit['picture'],
                'smallPicture' => (array) $unit['smallPicture'],
                'availableDates' => (string) $unit['availableDates'],
            ];
        }, $this->property['units']['unit']);
    }

    public function unitPhotosWithoutFirsOfUnits()
    {
        $photo = [];

        foreach ($this->units() as $unit) {
            foreach ($unit->picture as $k => $item) {
                if ($k === 0) {
                    continue;
                }

                $photo[] = [
                    'photo' => $item,
                    'smallPhoto' => isset($unit->smallPicture[$k]) && $unit->smallPicture[$k] ? $unit->smallPicture[$k] : $item
                ];
            }
        }

        return $photo;
    }

    public function manager()
    {
        return $this->property['manager'] ?: '';
    }

    public function amenities()
    {
        if (!isset($this->property['amenities']) && !isset($this->property['amenities']['amenity'])) {
            return [];
        }

        return $this->property['amenities']['amenity'];
    }

    public function routeUrl()
    {
        return $this->property['origin_url'] ?: '';
    }

    public function websiteUrl()
    {
        return isset($this->property['website_url']) ? (string) $this->property['website_url'] : '';
    }

    public function phone()
    {
        return !empty($this->property['phone']) ? $this->property['phone'] : '';
    }

    public function additionalParams()
    {
        return isset($this->property['additional_params']) && $this->property['additional_params']
            ? $this->property['additional_params']
            : '';
    }
}