<?php

namespace NetworkImport;

use Exception;

/**
 * Class NetworkSetting
 */
class NetworkSetting
{
    private static $availableImports;
    private static $currentImportName;
    private static $uploadFolder;
    private static $domainLimit;
    private static $subcategoryArray;

    public static function domainLimit()
    {
        if (!is_null(self::$domainLimit) && is_array(self::$domainLimit) && isset(self::$domainLimit[self::currentPoweredBy()])) {
            return self::$domainLimit[self::currentPoweredBy()];
        }

        if (!is_null(self::$domainLimit) && is_array(self::$domainLimit) && isset(self::$domainLimit[self::parseFile()])) {
            return self::$domainLimit[self::parseFile()];
        }

        return 300;
    }

    /**
     * @param $subcategory
     * @return string
     */
    public static function returnSubcategory($subcategory)
    {
        if (isset(self::$subcategoryArray[$subcategory])) {
            return self::$subcategoryArray[$subcategory];
        }

        return self::$subcategoryArray['default'];
    }


    /**
     * @return mixed
     */
    public static function currentPoweredBy()
    {
        return self::$availableImports[self::$currentImportName];
    }

    /**
     * @return mixed
     */
    public static function parseFile()
    {
        return trim(self::$currentImportName);
    }

    /**
     * @return mixed
     */
    public static function uploadFolder()
    {
        return self::$uploadFolder;
    }


    /**
     * @param $name
     * @param $uploadFolder
     */
    public static function init($name, $uploadFolder)
    {
        self::$currentImportName = $name;
        self::$uploadFolder = $uploadFolder;
    }

    /**
     * @param $importsArray
     */
    public static function initAvailableImports($importsArray)
    {
        self::$availableImports = $importsArray;
    }

    /**
     * @param $subcategoryArray
     * @param $default
     */
    public static function initSubcategory($subcategoryArray, $default = null)
    {
        if ($default) {
            $subcategoryArray['default'] = $default;
        }

        self::$subcategoryArray = $subcategoryArray;
    }

    /**
     * @throws Exception
     */
    public static function invariant()
    {
        if (!self::$subcategoryArray) {
            throw new Exception('Error: Please Install subcategory settings');
        }

        if (!isset(self::$subcategoryArray['default'])) {
            throw new Exception('Error: Please Install default subcategory in initSubcategory');
        }

        if (!self::$uploadFolder) {
            throw new Exception('Error: $uploadFolder is not exist');
        }

        if (!self::$currentImportName || !isset(self::$availableImports[self::$currentImportName])) {
            throw new Exception('Error: import name is not valid. Watch to initAvailableImports');
        }
    }

    public static function domainLimitSet($domainLimit)
    {
        self::$domainLimit = $domainLimit;
    }

}