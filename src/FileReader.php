<?php

namespace NetworkImport;

use Exception;
use XMLReader;

class FileReader
{
    private $xml;

    public function __construct($xml)
    {
        $this->xml = $xml;
    }

    /**
     * @throws Exception
     */
    public static function getContent($file)
    {
        $api = new NetworkApi();

        if (!class_exists('XMLReader')) {
            throw new Exception('Class XMLReader is not exist');
        }

        $xml = new XMLReader();
        if (!$xml->open($api->feedImportFileApiLink($file))) {
            throw new Exception("Error: $file was not opening");
        }

        return new static($xml);
    }


    public function parse($callback)
    {
        while ($this->xml->read()) {
            if ($this->xml->nodeType == XMLReader::ELEMENT && $this->xml->name == 'ad') {
                $product_xml = $this->xml->readOuterXml();
                $val_element = simplexml_load_string($product_xml, 'SimpleXMLElement', LIBXML_NOBLANKS && LIBXML_NOWARNING);
                $val_element = json_decode(json_encode($val_element), true);
                $callback(new NetworkParser($val_element));
            }
        }
    }


}