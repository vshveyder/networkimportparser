# NETWORK IMPORT PARSER
****
**Private Library**
****
This Library using for parse xml file with network.uloop.com
for [uloop](https://www.uloop.com), [collegerentals](https://www.uloop.com), [collegestudentsapartments](https://www.uloop.com)
****


````php
require_once 'vendor/autoload.php';

use NetworkImport\NetworkSetting;
use NetworkImport\NetworkParser;
use NetworkImport\FileReader;

/* INIT SETTINGS */
NetworkSetting::init($argv[1], 'path to upload file ');
NetworkSetting::initAvailableImports([ /*'NAME_OF_IMPORT' => ITEM_POWERED_BY, ...*/ ]);
NetworkSetting::domainLimitSet([]);
NetworkSetting::initSubcategory([ /*'NAME_OF_IMPORT' => SUBCATEGORY_ID,*/ ], DEFAULT_VALUE);
/* INIT SETTINGS END */

try {
    NetworkSetting::invariant(); // check, all fields installed
} catch (Exception $exception) {}

try {
    $fileReader = FileReader::getContent(NetworkSetting::parseFile());
} catch (Exception $e) {exit();}

$fileReader->parse(function (NetworkParser $parser) {
    // processing of results
});
````

`$argv[1] - NAME_OF_IMPORT`

### **To connect the library to the project**

Add this code to composer.json

For example

````json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://vshveyder@bitbucket.org/vshveyder/networkimportparser.git"
    },
    {
      "type": "package",
      "package": {
        "name": "accumulator/network-import-parser",
        "version": "dev-master",
        "dist": {
          "url": "https://vshveyder@bitbucket.org/vshveyder/networkimportparser.git",
          "type": "git"
        }
      }
    }
  ],
  "require": {
    "accumulator/network-import-parser": "^1.2"
  }
}
````